# Algoritmos geneticos
'''
TSP: Travel Sales Person

- Maximizar/minimizar

- Individuos:
    Camino o ruta, pasando por todas las ciudades.
    Suponemos 5 ciudades:   |1|2|3|4|5|
                            |4|2|5|1|3|

- Poblacion
- Generaciones
- Evolucion de la poblacion:
-    Seleccion --> Fitness
-    Mutacion
-    Cruza (Crossover)

.
'''

from math import sqrt
from random import shuffle, randint, random

class City():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "{}-{}".format(self.x, self.y) 

    def distanceTo(self, city):
        dx = self.x - city.x
        dy = self.y - city.y
        return sqrt(dx * dx + dy * dy)

class Individuo():
    def __init__(self, cities):
        self.route = cities

    def __str__(self):
        return ', '.join(map(str, self.route))

    def distance(self):
        dis = 0
        for i in range(len(self.route) - 1):
            dis += self.route[i].distanceTo(self.route[i + 1])
        
        # Calculo desde la ultima ciudad a la ciudad de origen
        dis += self.route[-1].distanceTo(self.route[0])

        return dis

class Poblacion():

    def __init__(self):
        self.individuos = []

    def get_mejor(self):
        mejor = self.individuos[0]
        for i in self.individuos[1:]:
            if mejor.distance > i.distance():
                mejor = i
        
        return mejor

    def __str__(self):
        s = []
        for i in self.individuos:
            s.append(str(i) + '\t' + str(i.distance()))

        return '\n'.join(s)

class AG():

    def __init__(self, cities, pob_size = 10, max_iter = 10000):
        self.cities = cities
        self.pob_size = pob_size
        self.max_iter = max_iter
        self.ini_pob = []
        self.mutation_rate = 0.1
        self.crossover_rate = 0.9
    
    def crossover(self, ind1, ind2):
        start = randint(0, len(ind1.route) - 1)
        end = randint(start, len(ind1.route))
        
        individuo = Individuo(ind1.route[start:end].copy())

        for city in ind2.route:
            if not city in individuo.route:
                individuo.route.append(city)

        return individuo

    def mutation(self, inv):
        individuo = Individuo(inv.route.copy())

        i = randint(0, len(individuo.route) - 1)
        j = randint(0, len(individuo.route) - 1)
        while j == i:
            j = randint(0, len(individuo.route) - 1)
        
        city = individuo.route[i]
        individuo.route[i] = individuo.route[j]
        individuo.route[j] = city

        return individuo
    
    def compute_converge(self, prev, current):
        sum_p = 0
        for ind in prev.individuos:
            sum_p += ind.distance()

        sum_c = 0
        for ind in current.individuos:
            sum_c += ind.distance()

        dif = abs(sum_p - sum_c)

        return dif < 0.00001 

    def start(self):
        # Crear la poblacion inicial
        self.ini_pob = Poblacion()
        for i in range(self.pob_size):
            c = self.cities.copy()
            shuffle(c)
            self.ini_pob.individuos.append(Individuo(c))

        print('-'*50)
        print("Poblacion Inicial:")
        print(self.ini_pob)
        print('-'*50)

        
        # Comienza la iteracion de generaciones
        current_pob = Poblacion()
        current_pob.individuos = self.ini_pob.individuos.copy()

        generation = 0
        converge_count = 0

        while generation < self.max_iter and converge_count < self.max_iter * 0.15:
            generation += 1

            new_pob = Poblacion()
            
            # Todos los ind. actuales pasan "temporalmente" a la nueva
            # poblacion
            for ind in current_pob.individuos:
                new_pob.individuos.append(ind)

            if random() < self.crossover_rate:
                print('**** Crossover ****')
                i1 = current_pob.individuos[randint(0, len(current_pob.individuos)-1)]
                i2 = current_pob.individuos[randint(0, len(current_pob.individuos)-1)]

                new_individuo = self.crossover(i1, i2)
                new_pob.individuos.append(new_individuo)

            if random() < self.mutation_rate:
                print('**** Mutation ****')
                i = current_pob.individuos[randint(0,len(current_pob.individuos)-1)]
                
                new_individuo = self.mutation(i)
                new_pob.individuos.append(new_individuo)
            

            new_pob.individuos = sorted(new_pob.individuos, key=lambda i: i.distance())

            # Elitista. Se eliminan aquellos individuos considerados
            # "malos" para la poblacion
            new_pob.individuos = new_pob.individuos[:self.pob_size]
            
            print("Poblacion generacion " + str(generation))
            print(new_pob)
            print('-'*50)

            converge = self.compute_converge(current_pob, new_pob)
            if converge:
                converge_count += 1
            else:
                converge_count = 0

            current_pob = new_pob
            # Fin while


cities = []
f = open('cities.txt', 'r')
lines = f.readlines()
f.close()

for line in lines:
    coord = line.split(',')
    cities.append(City(int(coord[0]), int(coord[1])))

ag = AG(cities, 5)
ag.start()