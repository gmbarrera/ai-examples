from sklearn.datasets import make_classification
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import load_iris


data, target = make_classification(n_samples=1000, n_features=5, n_informative=3, 
                                    n_classes=3)

#for d, t in zip(data, target):
#    print(str(d) + '\t' + str(t))
'''
classifier = RandomForestClassifier(n_estimators=100, max_depth=2)

classifier.fit(data, target)

print(classifier.feature_importance)

pre = classifier.predict([[0.5, 0.1, -0.2, 0, 0.23]])
print(pre)
'''

iris = load_iris()

classifier = RandomForestClassifier(n_estimators=10, max_depth=2)
classifier.fit(iris.data, iris.target)

pre = classifier.predict_proba([[5.09, 3.52, 1., 0.201]])
print(pre)

