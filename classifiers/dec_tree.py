# Sci-kit Learn
from sklearn import tree
import graphviz

# Se crea la instancia del clasificador en base a Arboles de Decision
classifier = tree.DecisionTreeClassifier()

# Datos de entrenamiento, cada elemento de la lista es una lista de datos
# de los features observados, en orden todos los elementos

# Features: altura, peso, numero calzado
data = [
    [181, 80, 44], [177, 70, 43], [160, 60, 38], [154, 54, 37], 
    [190, 90, 47], [175, 64, 39], [166, 65, 40],
    [177, 70, 40], [159, 55, 37], [171, 75, 42], [181, 85, 43]
]

target = [
    'hombre', 'hombre', 'mujer', 'mujer', 'hombre', 'hombre', 'mujer', 'mujer',
    'mujer', 'hombre', 'hombre'
]

# Con los datos se procede a entrenar el clasificador
classifier_trained = classifier.fit(data, target)

# Se usa un dato para que los clasifique
test1 = [178, 70, 42]

predic = classifier_trained.predict([test1])
print(predic)

# Arma los datos para plotear a partir del arbol de decision entrenado
dot_data = tree.export_graphviz(classifier_trained, out_file=None, 
                                filled=True, rounded=True)

graph = graphviz.Source(dot_data, format='png')
graph.render('Hombre-Mujer', view=True)