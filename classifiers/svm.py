# Support Vector Machine

from sklearn import svm
from sklearn.datasets import load_iris
import pickle


clf = svm.SVC(gamma='scale')

iris = load_iris()

X, y = iris.data, iris.target

clf.fit(X, y)

s = pickle.dumps(clf)   

clf2 = pickle.loads(s)
print(clf2.predict(X[60:61]))
