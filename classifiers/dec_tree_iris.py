from sklearn import tree
from sklearn.datasets import load_iris

import graphviz

iris = load_iris()

# Features: Largo de sépalo 	
#           Ancho de sépalo
#  	        Largo de pétalo
#        	Ancho de pétalo 	

# 0- I. setosa
# 1- I. versicolor
# 2- I. virginica

classifier = tree.DecisionTreeClassifier()

classifier_trained = classifier.fit(iris.data, iris.target)

test1 = [5.09, 3.52, 1., 0.201]

pred = classifier_trained.predict_proba([test1])
print(pred)

dot_data = tree.export_graphviz(classifier_trained, out_file=None,
                                filled=True, rounded=True)

graph = graphviz.Source(dot_data)
graph.render('Iris', view=True)