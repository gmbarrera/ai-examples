# Gaussian naive Bayes

import numpy as np
from sklearn.naive_bayes import GaussianNB

X = np.array([[-1, -1] ,[-2,-1], [-3, -2], [1, 1], [2, 1], [3, 2] ])
Y = np.array([1, 1, 1, 2, 2, 2])

clf = GaussianNB()
clf.fit(X, Y)

print(clf.predict( [ [-0.8, -1]   ] ))
print(clf.predict_proba( [ [-1, -1]   ] ))
print(clf.predict_proba( [ [-1, -1]   ] ).round())



# EJEMPLO 2

from sklearn.datasets import load_iris
from sklearn.utils import shuffle
iris = load_iris()
X, y = shuffle(iris.data, iris.target, random_state=0)

# Guardo los primeros 20 elementos para validacion
X_true = X[:30]
y_true = y[:30]

# Los 130 restantes elementos para entrenar
X_learn = X[30:]
y_learn = y[30:]

clf = GaussianNB()
clf.fit(X_learn, y_learn)

y_predict = clf.predict(X_true)

print(y_true)
print(y_predict)

from sklearn.metrics import confusion_matrix

cm = confusion_matrix(y_true, y_predict)
print(cm)
