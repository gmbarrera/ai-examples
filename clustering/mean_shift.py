import numpy as np
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn.datasets.samples_generator import make_blobs
import matplotlib.pyplot as plt
from itertools import cycle


centers = [[1,1], [-1,-1], [1, -1], [-1, 1]]
X, _ = make_blobs(n_samples=10000, centers=centers, cluster_std=0.6)

bandwidth = estimate_bandwidth(X, quantile=0.2, n_samples=500)

ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)
ms.fit(X)

labels = ms.labels_
labels_unique = np.unique(labels)
cluster_centers = ms.cluster_centers_

print(cluster_centers)
print(labels_unique)

plt.figure(1)
plt.clf()

colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
for k, col in zip(range(len(labels_unique)), colors):
    m = labels==k
    cluster_center = cluster_centers[k]
    plt.plot(X[m, 0], X[m, 1], col + '.')
    plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
          markeredgecolor='k', markersize=14)

plt.title('Meanshift')
plt.show()