from sklearn.cluster import KMeans
from sklearn.datasets import load_iris
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

iris = load_iris()
X = iris.data
y = iris.target

k1 = KMeans(n_clusters=10).fit(X)

print(k1.labels_)

fig = plt.figure(0, figsize=(4, 3))
ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=130)
labels = k1.labels_
ax.scatter(X[:,3], X[:, 0], X[:, 2], c=labels.astype(np.float), edgecolor='b')
ax.w_xaxis.set_ticklabels([])
ax.w_yaxis.set_ticklabels([])
ax.w_zaxis.set_ticklabels([])
ax.set_xlabel('Petal width')
ax.set_ylabel('Setal length')
ax.set_zlabel('Petal length')
ax.set_title('Kmeans n=3')
ax.dist = 12

plt.show()
