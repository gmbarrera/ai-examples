from sklearn.datasets import load_iris
from sklearn.linear_model import LogisticRegression

X, y = load_iris(return_X_y=True)

clf = LogisticRegression(random_state=0, solver='lbfgs', 
                    multi_class='multinomial', max_iter=500)

clf.fit(X, y)

#Features: Largo de sépalo 	
#           Ancho de sépalo
#  	        Largo de pétalo
#        	Ancho de pétalo 	

# 0- I. setosa
# 1- I. versicolor
# 2- I. virginica

print([6.2, 4.1, 4.1, 2.7])
print(clf.predict([[6.2, 4.1, 4.1, 2.7]]))

print(clf.score(X, y))