from sklearn.datasets import load_digits
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
import seaborn as sns


digits = load_digits()

print(digits.data.shape)    # 1797 Imagenes de 8x8 pixeles

print(digits.target.shape)

print(digits.data[0])
print(digits.target[0])

plt.figure(figsize=(20,4))
for index, (image, label) in enumerate(zip(digits.data[10:15], digits.target[10:15])):
    plt.subplot(1, 5, index + 1)
    plt.imshow(np.reshape(image, (8,8)), cmap=plt.cm.gray)
    plt.title('Digito: %i\n' % label, fontsize=20)

#plt.show()



x_train, x_test, y_train, y_test = train_test_split(digits.data, digits.target, 
                          test_size=0.25)

print(len(x_test))
print(y_train[:10])

lg = LogisticRegression(random_state=0, solver='lbfgs', 
                    multi_class='multinomial', max_iter=5000)  # Se crea el clasificador
lg.fit(x_train, y_train)

predictions = lg.predict(x_test)

cm = metrics.confusion_matrix(y_test, predictions)

print(cm)

plt.figure(figsize=(9,9))
sns.heatmap(cm, annot=True, fmt='.3f', linewidths=.5, square=True, cmap='Blues_r')
plt.ylabel('Real')
plt.xlabel('Predicted')
all_sample_title = 'Score: {0}'.format(lg.score(digits.data, digits.target))
plt.title(all_sample_title, size=16)
plt.show()
# plt.savefig('sarasa.png')

'''

x = [1,2,3,4,5]
y = [a,b,c,d,e]

zip(x,y) --> [(1,a), (2,b), (3, c), (4,d), (5,e)]

enumerate(y) --> 0,a    1,b    2,c     (iterador)  

'''