import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score



diabetes = datasets.load_diabetes()

diabetes_X = diabetes.data[:, np.newaxis, 2]    # BMI

X_train = diabetes_X[:-20]      # Los ultimos veinte
X_test = diabetes_X[-20:]       # No tomo los ultimos veinte

y_train = diabetes.target[:-20]
y_test = diabetes.target[-20:]

regr = linear_model.LinearRegression()

regr.fit(X_train, y_train)

y_pred = regr.predict(X_test)

print('Coef: ', regr.coef_)

print('Error cuad: %.2f' % mean_squared_error(y_test, y_pred))

print(y_pred[:10])
print(X_test[4])
print(y_test[4])

plt.scatter(X_test, y_test, color='red')
plt.plot(X_test, y_pred, color='blue', linewidth=3)
plt.xticks(())
plt.yticks(())
plt.show()
