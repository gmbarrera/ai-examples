# Armado del entorno

Creamos el entorno virtual de python:

```
virtualenv env     
```

Activamos el entorno virtual

Windows:
```
source env/Scripts/activate
```

Linux/iOS:
```
source env/bin/activate
```

Instalar los paquetes necesarios:
```
pip install sklearn
pip install graphviz
```

Instalar: https://graphviz.gitlab.io/download/




# Clasificadores

## Árboles de Decisión

Clase a utilizar: sklearn.tree.DecissionTreeClassifier


## K-Means


## Mean-shift

