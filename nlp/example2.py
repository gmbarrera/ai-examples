import nltk
from nltk.corpus import stopwords, wordnet
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import PorterStemmer, SnowballStemmer
from nltk.stem import WordNetLemmatizer


text = '''Stanley Martin Lieber was born on December 
28, 1922, in Manhattan, New York City,[2] in the 
apartment of his Romanian-born Jewish immigrant parents,
 Celia (née Solomon) and Jack Lieber, at the corner of 
 West 98th Street and West End Avenue in Manhattan.[3][4] His father, trained as a dress cutter, worked only sporadically after the Great Depression,[3] and the family moved further uptown to Fort Washington Avenue,[5] in Washington Heights, Manhattan. Lee had one younger brother named Larry Lieber.[6] He said in 2006 that as a child he was influenced by books and movies, particularly those with Errol Flynn playing heroic roles.[7] By the time Lee was in his teens, the family was living in an apartment at 1720 University Avenue in The Bronx. Lee described it as "a third-floor apartment facing out back". Lee and his brother shared the bedroom, while their parents slept on a foldout couch.[6]
Lee attended DeWitt Clinton High School in the Bronx.[8] In his youth, Lee enjoyed writing, and entertained dreams of one day writing the "Great American Novel".[9] He said that in his youth he worked such part-time jobs as writing obituaries for a news service and press releases for the National Tuberculosis Center;[10] delivering sandwiches for the Jack May pharmacy to offices in Rockefeller Center; working as an office boy for a trouser manufacturer; ushering at the Rivoli Theater on Broadway;[11] and selling subscriptions to the New York Herald Tribune newspaper.[12] He graduated from high school early, aged 16½ in 1939, and joined the WPA Federal Theatre Project.[13] '''

tokens = [t for t in text.split()]

freq = nltk.FreqDist(tokens)
freq.plot(20, cumulative=False)

clean_tokens = tokens[:]    # copiar la lista

st = stopwords.words('english')
for token in tokens:
    if token in st:
        clean_tokens.remove(token)

freq = nltk.FreqDist(clean_tokens)
freq.plot(20, cumulative=False)

# Se separa por oraciones
sentences = sent_tokenize(text)
for s in sentences:
    print('*'*50)
    print(s)

# Se separa por palabras
words = word_tokenize(sentences[0])
print(words)

print('*'*50)
syn = wordnet.synsets('easy')
print(syn[0].definition())

print('*'*50)
for syn in wordnet.synsets('easy'):
    for lemma in syn.lemmas():
        if lemma.antonyms():
            print(lemma.antonyms()[0].name())

print('*'*50)

stemmer = PorterStemmer()
print(stemmer.stem('increases'))

print('*'*50)

sp_stemmer = SnowballStemmer('spanish')
print(sp_stemmer.stem('cocinando'))

print('*'*50)
lemmatizer = WordNetLemmatizer()
print(lemmatizer.lemmatize('increases'))
