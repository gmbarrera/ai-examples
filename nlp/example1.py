# NLP: Procesamiento del Lenguaje Natural
import nltk

'''
desde la consola de python:
import nltk
nltk.download('averaged_perceptron')
nltk.download('punkt')
nltk.download('maxent_ne_chunker')
nltk.download('words')
nltk.download('treebank')
nltk.download('stopwords')
nltk.download('wordnet')
'''

sentence = '''The cow gives milk to us every 
  morning, but we eat chocolates.
  '''

tokens = nltk.word_tokenize(sentence)
print(tokens)

tagged = nltk.pos_tag(tokens)
print(tagged)

entities = nltk.chunk.ne_chunk(tagged)
print(entities)

# from nltk.corpus import treebank
# t = treebank.parsed_sents('wsj_0001.mrg')[0]
# t.draw()